# proj3-JSA
Vocabulary anagrams game for primary school English language learners (ELL)
By: Sophie Ahlberg
Email: sophieahlberg92@gmail.com

## Overview

A simple anagram game designed for English-language learning students in elementary and middle school. Students are presented with a list of vocabulary words (taken from a text file) and an anagram. The anagram is a jumble of some number of vocabulary words, randomly chosen. Students attempt to type words that can be created from the jumble. When a matching word is typed, it is added to a list of solved words. 

The vocabulary word list is fixed for one invocation of the server, so multiple students connected to the same server will see the same vocabulary list but may  have different anagrams.

## Authors 

Initial version by M Young; Docker version added by R Durairajan. 


* This word game uses AJAX interaction on each keystroke with appropriate responses and web actions
* To activate website, in your terminal, use docker build -t anynameworkshere . (don't forget the dot!)
* To run website, write: docker run -d -p 5000:5000 anynameworkshere
* The website will be up and running on http://127.0.0.1:5000/



# Basic Docker commands

* Command to get information about docker setup on your machine

  ```
  docker info
  ```

* List of docker containers running can be found using

  ```
  docker ps -a
  ```

* Remove containers using

  ```
  sudo docker rm <Container Name>
  ```

* To run docker container use

  ```
  docker run -h CONTAINER1 -i -t debian /bin/bash
  docker run -h CONTAINER1 -i -t ubuntu /bin/bash
  ```

  Here, -h is used to specify a container name, -t to start with tty, and -i means interactive. Note: second times will be quick because of caching.

* Docker with networking

  ```
  docker run -h CONTAINER2 -i -t --net="bridge" debian /bin/bash
  ```

* When the containers are not running and when you're done, kill them using

  ```
  docker rm `docker ps --no-trunc -aq`
  ```

* Rename using

  ```
  docker rename name_v0 name_v1
  ```

* Start a container using

  ```
  docker start <container name>
  ```

* Stop a container using

  ```
  docker start <container name>


