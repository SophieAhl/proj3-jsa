"""
Flask web site with vocabulary matching game
(identify vocabulary words that can be made
from a scrambled string)
"""

import flask
import logging

# Our own modules
from letterbag import LetterBag
from vocab import Vocab
from jumble import jumbled
import config

###
# Globals
###
app = flask.Flask(__name__)

CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY  # Should allow using session variables

WORDS = Vocab(CONFIG.VOCAB)

###
# Pages
###

@app.route("/")
@app.route("/index")
def index():
    """The main page of the application"""
    flask.g.vocab = WORDS.as_list()
    flask.session["target_count"] = min(
        len(flask.g.vocab), CONFIG.SUCCESS_AT_COUNT)
    flask.session["jumble"] = jumbled(
        flask.g.vocab, flask.session["target_count"])
    flask.session["matches"] = []
    app.logger.debug("Session variables have been set")
    assert flask.session["matches"] == []
    assert flask.session["target_count"] > 0
    app.logger.debug("At least one seems to be set correctly")
    return flask.render_template('vocab.html')


@app.route("/keep_going")
def keep_going():
    """
    After initial use of index, we keep the same scrambled
    word and try to get more matches
    """
    flask.g.vocab = WORDS.as_list()
    return flask.render_template('vocab.html')


@app.route("/success")
def success():
    return flask.render_template('success.html')


@app.route("/_check", methods=["POST"])
def check():
    
    app.logger.debug("Entering check")

    # The data we need, from form and from cookie
    text = flask.request.form["attempt"]
    jumble = flask.session["jumble"]
    matches = flask.session.get("matches", [])  # Default to empty list
    #length = len(text)
    length = len(text)
    letter = flask.request.form["attempt"]
    
    # Is it good?
    in_jumble = LetterBag(jumble).contains(text)
    matched = WORDS.has(text)

    if (length >0):
        return Countem()

###############
# AJAX request handlers
#   These return JSON, rather than rendering pages.
###############

@app.route ("/_countem")
def Countem ():
    text = flask.request.args.get("text", type=str)
    jumble = flask.session["jumble"]
    length = len(text)
    app.logger.debug("Got a JSON request")
    matches = flask.session.get("matches", [])  # Default to empty list
    in_jumble = LetterBag(jumble).contains(text)
    matched = WORDS.has(text)

    if length>= 6 and not matched:
        rslt = {"long_enough": length >= 6}
    
    elif matched and in_jumble and not (text in matches):
        rslt = {"ssval": text}
        matches.append(text)
        flask.session["matches"] = matches

    elif text in matches:
        rslt = {"tsval": text}

    elif not matched:
        rslt = {"usval": text}

    elif not in_jumble:
        rslt = {"vsval": text}
    else:
        app.logger.debug("This case shouldn't happen!")
        assert False

    if len(matches) >=flask.session["target_count"]:
        rslt = {"success": matches}

    return flask.jsonify(result = rslt)



#################
# Functions used within the templates
#################

@app.template_filter('filt')
def format_filt(something):
    """
    Example of a filter that can be used within
    the Jinja2 code
    """
    return "Not what you asked for"

###################
#   Error handlers
###################

@app.errorhandler(404)
def error_404(e):
    app.logger.warning("++ 404 error: {}".format(e))
    return flask.render_template('404.html'), 404


@app.errorhandler(500)
def error_500(e):
    app.logger.warning("++ 500 error: {}".format(e))
    assert not True  # I want to invoke the debugger
    return flask.render_template('500.html'), 500


@app.errorhandler(403)
def error_403(e):
    app.logger.warning("++ 403 error: {}".format(e))
    return flask.render_template('403.html'), 403

####

if __name__ == "__main__":
    if CONFIG.DEBUG:
        app.debug = True
        app.logger.setLevel(logging.DEBUG)
        app.logger.info(
            "Opening for global access on port {}".format(CONFIG.PORT))
        app.run(port=CONFIG.PORT, host="0.0.0.0")
